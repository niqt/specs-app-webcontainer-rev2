package specs.sws_application.frontend.rest;

import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.transform.Result;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import eu.specs.datamodel.agreement.offer.AgreementOffer;
import eu.specs.datamodel.agreement.slo.ServiceProperties;
import eu.specs.datamodel.agreement.slo.Variable;
import eu.specs.datamodel.agreement.terms.GuaranteeTerm;
import eu.specs.datamodel.agreement.terms.ServiceDescriptionTerm;
import eu.specs.datamodel.agreement.terms.ServiceReference;
import eu.specs.datamodel.agreement.terms.Term;
import eu.specs.datamodel.agreement.terms.Terms;
import eu.specs.datamodel.agreement.terms.Terms.All;
import eu.specs.datamodel.metrics.AbstractMetricType;
import eu.specs.datamodel.sla.sdt.CapabilityType;
import eu.specs.datamodel.sla.sdt.NISTcontrol;
import eu.specs.datamodel.sla.sdt.ObjectiveList;
import eu.specs.datamodel.sla.sdt.OneOpOperator;
import eu.specs.datamodel.sla.sdt.SLOType;
import eu.specs.datamodel.sla.sdt.ServiceDescriptionType;
import eu.specs.datamodel.sla.sdt.CapabilityType.ControlFramework;
import eu.specs.datamodel.sla.sdt.ServiceDescriptionType.Capabilities;
import eu.specs.datamodel.sla.sdt.WeightType;

import org.apache.commons.lang.math.NumberUtils;

import specs.sw_application.frontend.entities.CapabilitiesEU;
import specs.sw_application.frontend.entities.CapabilityEU;
import specs.sw_application.frontend.entities.CapabilitySLOStep;
import specs.sw_application.frontend.entities.ChoosedCapabilities;
import specs.sw_application.frontend.entities.CollectionType;
import specs.sw_application.frontend.entities.FrameworkEU;
import specs.sw_application.frontend.entities.FrameworkSLO;
import specs.sw_application.frontend.entities.IdList;
import specs.sw_application.frontend.entities.MetricEU;
import specs.sw_application.frontend.entities.MetricSLO;
import specs.sw_application.frontend.entities.SLOStep;
import specs.sw_application.frontend.entities.SecurityControlEU;
import specs.sw_application.frontend.entities.ServiceEU;
import specs.sw_application.frontend.entities.ServicesEU;
import specs.sws_application.utility.Utility;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@RestController
@RequestMapping("/")
@Configuration
@PropertySource("classpath:sws_conf.properties")
public class MainRestController {
	@Autowired
	Environment env;

	Map<String, String> servicesUrl = new HashMap<String, String>();
	Map<String, AgreementOffer> servicesTemplate = new HashMap<String, AgreementOffer>();
	Map<String, String> servicesXml = new HashMap<String, String>();
	
	@RequestMapping(value = "/capabilities/{service}", method = RequestMethod.GET)
	public ResponseEntity<CapabilitiesEU> capabilities(
			@PathVariable(value = "service") String service) {
		AgreementOffer offer = null;
		
		offer = getTemplateObject(service);
		if (!servicesTemplate.containsKey(service)) {;
			servicesTemplate.put(service, offer);
		}
		
		HttpHeaders responseHeaders = new HttpHeaders();
		responseHeaders.set("Access-Control-Allow-Origin",
				"http://localhost:8000");
		return new ResponseEntity<CapabilitiesEU>(getAllCapabilities(offer),
				responseHeaders, HttpStatus.OK);
	}

	@RequestMapping(value = "/services", method = RequestMethod.GET)
	public ResponseEntity<ServicesEU> services() {
		RestTemplate rest = new RestTemplate();
		CollectionType servers = (CollectionType) rest.getForObject(
				(String) env.getProperty("sla_template.endpoint"),
				CollectionType.class);

		List<ServiceEU> list = new ArrayList<ServiceEU>();

		for (int i = 0; i < servers.getItemList().size(); i++) {
			String id = Utility.lastElement(servers.getItemList().get(i)
					.getValue(), "/");
			ServiceEU service = new ServiceEU(id);
			list.add(service);
			servicesUrl.put(id, servers.getItemList().get(i).getValue());
		}

		ServicesEU services = new ServicesEU(list);
		HttpHeaders responseHeaders = new HttpHeaders();
		responseHeaders.set("Access-Control-Allow-Origin",
				"http://localhost:8000");
		return new ResponseEntity<ServicesEU>(services, responseHeaders,
				HttpStatus.OK);
	}

	@RequestMapping(value = "/securities", method = RequestMethod.POST, consumes = "application/json")
	public ResponseEntity<CapabilitiesEU> update(@RequestBody String ids) {
		CapabilitiesEU capabilitiesEU = null;
		ChoosedCapabilities capabilities;
		ObjectMapper mapper = new ObjectMapper();
		SLOStep sloStep = new SLOStep();
		try {
			capabilities = mapper.readValue(ids, ChoosedCapabilities.class);
			
			AgreementOffer offer = servicesTemplate
					.get(Utility.secondLastElement(capabilities.getIdsla(), "_"));
			capabilitiesEU = getCapabilities(offer,
					capabilities.getCapabilities());
		
			
		} catch (JsonParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JsonMappingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		HttpHeaders responseHeaders = new HttpHeaders();

		responseHeaders.set("Access-Control-Allow-Origin",
				"http://localhost:8000");

		return new ResponseEntity<CapabilitiesEU>(capabilitiesEU, responseHeaders, HttpStatus.OK);
	}

	@RequestMapping(value = "/agreements", method = RequestMethod.POST, consumes = "application/json")
	public ResponseEntity<SLOStep> agreement(@RequestBody String ids) {
		CapabilitiesEU capabilitiesEU = null;
		
		ObjectMapper mapper = new ObjectMapper();
		SLOStep sloStep = new SLOStep();
		try {
			capabilitiesEU = mapper.readValue(ids, CapabilitiesEU.class);
			AgreementOffer offer = servicesTemplate
					.get(Utility.secondLastElement(capabilitiesEU.getIdsla(), "_"));

			sloStep = buildSLOStep(offer, capabilitiesEU);
		
		} catch (JsonParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JsonMappingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		HttpHeaders responseHeaders = new HttpHeaders();

		responseHeaders.set("Access-Control-Allow-Origin",
				"http://localhost:8000");

		return new ResponseEntity<SLOStep>(sloStep,
				responseHeaders, HttpStatus.OK);		
	}
	
	@RequestMapping(value = "/overview", method = RequestMethod.POST, consumes = "application/json")
	public ResponseEntity<CollectionType> submitSla(@RequestBody String ids) {

		CapabilitiesEU capabilitiesEU = null;
		
		ObjectMapper mapper = new ObjectMapper();
		CollectionType offers = new CollectionType();

		try {
			capabilitiesEU = mapper.readValue(ids, CapabilitiesEU.class);
			String service = Utility.secondLastElement(capabilitiesEU.getIdsla(), "_");

			AgreementOffer overview = buildOverview(capabilitiesEU, service);
			System.out.println("CREATO");
			overview.setName(capabilitiesEU.getIdsla());
			RestTemplate rest = new RestTemplate();
			JAXBContext jaxbContext = JAXBContext.newInstance(AgreementOffer.class);
			Marshaller jaxbMarshaller = jaxbContext.createMarshaller();

			jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);

			java.io.StringWriter sw = new StringWriter();
			jaxbMarshaller.marshal(overview, sw);

			System.out.println("XML = " + sw.toString());
			
			String url = (String) env.getProperty("sla_template.endpoint") + capabilitiesEU.getIdsla() + "/slaoffers";
			
			HttpHeaders headers = new HttpHeaders();
			headers.setContentType(MediaType.TEXT_XML);

			HttpEntity entity = new HttpEntity(sw.toString(), headers);

			ResponseEntity<CollectionType> response= rest.exchange(url, HttpMethod.POST, entity
			    , CollectionType.class);
			offers = response.getBody();
			
		} catch (JsonParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JsonMappingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JAXBException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		HttpHeaders responseHeaders = new HttpHeaders();

		responseHeaders.set("Access-Control-Allow-Origin",
				"http://localhost:8000");

		return new ResponseEntity<CollectionType>(offers,
				responseHeaders, HttpStatus.OK);
	}
	

	public AgreementOffer buildOverview(CapabilitiesEU capabilitiesEU, String service) {
		String key = (String)(servicesTemplate.keySet().toArray())[0];

		AgreementOffer offer = buildOfferFromXml(servicesXml.get(service));

		All all = offer.getTerms().getAll();
		List<Term> terms = all.getAll();
		List<String> caps = getCapabilityId(capabilitiesEU);
		List<String> metricRefs = getMetriRef(capabilitiesEU);
		
		for (int i = 0; i < terms.size(); i++) {
			if (terms.get(i) instanceof GuaranteeTerm) {
				GuaranteeTerm guarantee = (GuaranteeTerm) terms.get(i);
				if (!caps.contains(Utility.getFromQuotes(guarantee.getName()))) {
					terms.remove(i);
					i--;
				} else {
					ObjectiveList objectiveList = guarantee.getServiceLevelObjective().getCustomServiceLevel().getObjectiveList();
					List<SLOType> slos = objectiveList.getSLO();
					int j = 0;
					while (j < slos.size()) {
						if (!metricRefs.contains(slos.get(j).getMetricREF()))
							slos.remove(j);
						else {
							MetricSLO metricSLO = getMetricValue(capabilitiesEU, Utility.getFromQuotes(guarantee.getName()), slos.get(j).getMetricREF());
							if (metricSLO != null) {
								slos.get(j).setImportanceWeight(WeightType.valueOf(metricSLO.getImportance()));
								slos.get(j).getSLOexpression().getOneOpExpression().setOperand(metricSLO.getOperand());
								slos.get(j).getSLOexpression().getOneOpExpression().setOperator(Utility.convertTOSLOOperator(metricSLO.getOperator()));
							}
							j++;
						}
					}
				}
			} else if (terms.get(i) instanceof ServiceDescriptionTerm) {
				ServiceDescriptionTerm serviceDescTerm = (ServiceDescriptionTerm) terms
						.get(i);
				ServiceDescriptionType servDesc = serviceDescTerm
						.getServiceDescription();
				Capabilities capabilities = servDesc.getCapabilities();

				List<CapabilityType> capabilityList = capabilities
						.getCapability();

				servDesc.getSecurityMetrics().getSecurityMetric().get(0).getReferenceId();
				
				boolean found = false;
				int j = 0;
				while (j < capabilityList.size()) {
					found = false;
					for (int k = 0; k < capabilitiesEU.getCapabilities().size(); k++) {
						if (capabilityList.get(j).getId().equals(capabilitiesEU.getCapabilities().get(k).getId())) {
							found = true;
							ControlFramework controlFramework = capabilityList.get(j).getControlFramework();
							updateFramework(controlFramework, capabilitiesEU.getCapabilities().get(k).getFrameworks().get(0));
							break;
						}
					}
					if (!found) {
						capabilityList.remove(j);
						//remove from <wsag:ServiceProperties wsag:Name="//specs:capability[@id='WEBPOOL']"
					} else {
						j++;
					}
					
				}				
				int k = 0;
				while (k < servDesc.getSecurityMetrics().getSecurityMetric().size()) {
					if (!metricRefs.contains(servDesc.getSecurityMetrics().getSecurityMetric().get(k))) {
						servDesc.getSecurityMetrics().getSecurityMetric().remove(k);
					} else {
						k++;
					}
				}
				
			} else if (terms.get(i) instanceof ServiceProperties) {
				ServiceProperties properties = (ServiceProperties) terms.get(i);
				if (!caps.contains(Utility.getFromQuotes(properties.getName()))) {
					terms.remove(i);
					i--;
				} else {
					properties.getVariableSet().getVariables();
					int j = 0;
					while (j < properties.getVariableSet().getVariables().size()) {
						if (! metricRefs.contains(properties.getVariableSet().getVariables().get(j).getMetric())) {
							properties.getVariableSet().getVariables().remove(j);
						} else {
							j++;
						}
					}
				}
			}
		}
		return offer;
	}
		
	public MetricSLO getMetricValue(CapabilitiesEU capabilitiesEU, String capabilityId, String metricId) {
		List<CapabilityEU> capabilities = capabilitiesEU.getCapabilities();
		for (int i = 0; i < capabilities.size(); i++) {
			if (capabilities.get(i).getId().equals(capabilityId)) {
				List<SecurityControlEU> controls = capabilities.get(i).getFrameworks().get(0).getSecurityControls();
				for (int j = 0; j < controls.size(); j++) {
					for (int k = 0; k < controls.get(j).getMetrics().size(); k++) {
						if (controls.get(j).getMetrics().get(k).getId().equals(metricId))
							return controls.get(j).getMetrics().get(k);
					}
				}
			}
		}
		return null;
	}
	
	public void updateFramework(ControlFramework control, FrameworkEU fameworkEU) {
		List <NISTcontrol> securities = control.getSecurityControl();
		List <SecurityControlEU> controls = fameworkEU.getSecurityControls();
		int i = 0;
		while (i < securities.size()) {
			boolean found = false;
			for (int j  = 0; j < controls.size(); j++) {
				if (controls.get(j).getId().equals(securities.get(i).getId())) {
					found = true;
					securities.get(i).setImportanceWeight(WeightType.fromValue(controls.get(j).getWeight()));
					
					break;
				}
			}
			if (!found)
				securities.remove(i);
			else
				i++;
		}
	}
		
	
	public List<String> getMetriRef(CapabilitiesEU capabilitiesEU) {
		List<String> refs = new ArrayList<String>();
		for (int i = 0; i < capabilitiesEU.getCapabilities().size(); i++) {
			List <SecurityControlEU> controls = capabilitiesEU.getCapabilities().get(i).getFrameworks().get(0).getSecurityControls();
			for (int j = 0; j < controls.size(); j++) {
				for (int k = 0; k < controls.get(j).getMetrics().size(); k++) {
					refs.add(controls.get(j).getMetrics().get(k).getId());
				}
			}
		}
		return refs;
	}
	
	public List<String> getCapabilityId(CapabilitiesEU capabilitiesEU) {
		List<String> caps = new ArrayList<String>();
		for (int i = 0; i < capabilitiesEU.getCapabilities().size(); i++) {
			caps.add(capabilitiesEU.getCapabilities().get(i).getId());
		}
		return caps;
	}
	
	
	public Map<String, MetricEU> getMetricBase(AgreementOffer agreement, CapabilityEU capability) {
		All all = agreement.getTerms().getAll();
		List<Term> terms = all.getAll();
		Map<String, MetricEU> metricsMap = new HashMap<String, MetricEU>();
		for (int i = 0; i < terms.size(); i++) {
			if (terms.get(i) instanceof ServiceProperties) {
				ServiceProperties serv = (ServiceProperties) terms.get(i);
				String name = serv.getName().substring(
						serv.getName().indexOf("'") + 1,
						serv.getName().lastIndexOf("'"));
				
				if (name.equals(capability.getId())) {
					List<Variable> variables = serv.getVariableSet()
							.getVariables();
					MetricEU metric;
					for (int j = 0; j < variables.size(); j++) {
						Variable var = variables.get(j);

						if (metricsMap.containsKey(var.getMetric())) {
							metric = metricsMap.get(var.getMetric());

						} else {
							metric = new MetricEU();
							//metric.setName(var.getName());
							metric.setId(var.getMetric());
						}
						String[] securities = var.getLocation().split("\\|");
						for (int k = 0; k < securities.length; k++) {
							String sec = securities[k].substring(
									securities[k].indexOf("'") + 1,
									securities[k].lastIndexOf("'"));
							
							if (!metric.getSecurityControls().contains(sec) && Utility.containSecurityControl(capability.getFrameworks().get(0).getSecurityControls(), sec) ) {
								List<String> sc = metric.getSecurityControls();
								sc.add(sec);
								metric.setSecurityControls(sc);
								
							}
						}
						metricsMap.put(var.getMetric(), metric);
					}
				}
			}
		}
		return metricsMap;
	}

	SLOStep buildSLOStep(AgreementOffer offer, CapabilitiesEU capabilitiesEU) {
		SLOStep sloStep = null;
		List<CapabilitySLOStep> capabilitiesSLOStep = new ArrayList<CapabilitySLOStep>();
		for (int i = 0; i < capabilitiesEU.getCapabilities().size(); i++) {
			CapabilityEU capability = capabilitiesEU.getCapabilities().get(i);
			CapabilitySLOStep current = new CapabilitySLOStep();
			current.setId(capability.getId());
			current.setName(capability.getId());
			Map<String, MetricEU> metrics = getMetricBase(offer, capability);
			addSLOInfo(offer, metrics);
			addMetricDefinition(offer, metrics);
			FrameworkSLO framework = new FrameworkSLO();
			framework.setId(capability.getFrameworks().get(0).getId());
			List<MetricEU> values = new ArrayList<MetricEU>(metrics.values());
			framework.setMetrics(values);
			List<FrameworkSLO> frameworks = new ArrayList<FrameworkSLO>();
			frameworks.add(framework);
			current.setFrameworks(frameworks);
			capabilitiesSLOStep.add(current);
		}
		sloStep = new SLOStep();
		sloStep.setIdsla(capabilitiesEU.getIdsla());
		sloStep.setCapabilities(capabilitiesSLOStep);
		
		return sloStep;
	}
	
	public void addSLOInfo(AgreementOffer offer, Map<String, MetricEU> metrics) {
		All all = offer.getTerms().getAll();
		List<Term> terms = all.getAll();
		for (int i = 0; i < terms.size(); i++) {
			if (terms.get(i) instanceof GuaranteeTerm) {
				GuaranteeTerm g = (GuaranteeTerm) terms.get(i);
				ObjectiveList objectiveList = g.getServiceLevelObjective()
						.getCustomServiceLevel().getObjectiveList();
				List<SLOType> slos = objectiveList.getSLO();
				for (int j = 0; j < slos.size(); j++) {
					
					if (metrics.containsKey(slos.get(j).getMetricREF())) {
						
						SLOType slo = slos.get(j);
						MetricEU metric = metrics.get(slos.get(j)
								.getMetricREF());
						if (metric == null)
							metric = new MetricEU();

						String operator = slo.getSLOexpression()
								.getOneOpExpression().getOperator().toString();
						String operand = slo.getSLOexpression()
								.getOneOpExpression().getOperand().toString();
						String importance = slo.getImportanceWeight().name();
						metric.setOperand(operand);
						metric.setOperator(Utility.getSLOOperand(operator));
						metric.setImportance(importance);
						metrics.put(slos.get(j).getMetricREF(), metric);
					}
				}
			}
		}
	}

	public FrameworkEU getFramework(CapabilityType capability) {
		ControlFramework controlFramework = capability.getControlFramework();
		List<SecurityControlEU> securityControl = new ArrayList<SecurityControlEU>();
		for (int i = 0; i < controlFramework.getSecurityControl().size(); i++) {
			NISTcontrol control = controlFramework.getSecurityControl().get(i);
			SecurityControlEU current = new SecurityControlEU(control.getId(),
					control.getName(), control.getControlDescription(), control
							.getImportanceWeight().value());
			// securityControl.add(current);
		}
		return new FrameworkEU(controlFramework.getId(),
				controlFramework.getFrameworkName(), securityControl);
	}

	public FrameworkEU getSecurities(CapabilityType capability) {
		ControlFramework controlFramework = capability.getControlFramework();
		List<SecurityControlEU> securityControl = new ArrayList<SecurityControlEU>();
		for (int i = 0; i < controlFramework.getSecurityControl().size(); i++) {
			NISTcontrol control = controlFramework.getSecurityControl().get(i);
			SecurityControlEU current = new SecurityControlEU(control.getId(),
					control.getName(), control.getControlDescription(), control
							.getImportanceWeight().value());
			securityControl.add(current);
		}
		return new FrameworkEU(controlFramework.getId(),
				controlFramework.getFrameworkName(), securityControl);
	}

	private AgreementOffer getTemplateObject(String name) {
		AgreementOffer offer = null;
		if (servicesUrl.containsKey(name)) {

			RestTemplate rest = new RestTemplate();
			String xml = (String) rest.getForObject(servicesUrl.get(name),
					String.class);
			servicesXml.put(name, xml);
			offer = buildOfferFromXml(xml);
		}
		return offer;
	}

	private AgreementOffer buildOfferFromXml(String xml) {
		AgreementOffer offer = null;
		try {
			JAXBContext jc = JAXBContext.newInstance(AgreementOffer.class);
			Unmarshaller unmarshaller = jc.createUnmarshaller();
			offer = (AgreementOffer) unmarshaller
					.unmarshal(new StringReader(xml));
		} catch (JAXBException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return offer;
	}
	
	private void addMetricDefinition(AgreementOffer agreement,
			Map<String, MetricEU> metrics) {
		All all = agreement.getTerms().getAll();
		List<Term> terms = all.getAll();
		for (int i = 0; i < terms.size(); i++) {
			if (terms.get(i) instanceof ServiceDescriptionTerm) {
				ServiceDescriptionTerm serviceDescTerm = (ServiceDescriptionTerm) terms
						.get(i);
				ServiceDescriptionType servDesc = serviceDescTerm
						.getServiceDescription();
				List<AbstractMetricType> metricType = servDesc
						.getSecurityMetrics().getSecurityMetric();
				for (int j = 0; j < metricType.size(); j++) {
					if (metrics.containsKey(metricType.get(j).getReferenceId())) {
						MetricEU metric = metrics.get(metricType.get(j)
								.getReferenceId());
						if (metric == null)
							metric = new MetricEU();
						metric.setDescription(metricType.get(j).getAbstractMetricDefinition().getExpression());
						metric.setName(metricType.get(j).getName());
						metrics.put(metricType.get(j).getReferenceId(), metric);
					}
				}
			}
		}
	}

	private CapabilitiesEU getAllCapabilities(AgreementOffer agreement) {
		List<CapabilityEU> listCapabilityEU = new ArrayList<CapabilityEU>();
		All all = agreement.getTerms().getAll();
		List<Term> terms = all.getAll();
		CapabilitiesEU capabilitiesEU = null;
		for (int i = 0; i < terms.size(); i++) {
			if (terms.get(i) instanceof GuaranteeTerm) {
				GuaranteeTerm g = (GuaranteeTerm) terms.get(i);
			} else if (terms.get(i) instanceof ServiceDescriptionTerm) {
				ServiceDescriptionTerm serviceDescTerm = (ServiceDescriptionTerm) terms
						.get(i);
				ServiceDescriptionType servDesc = serviceDescTerm
						.getServiceDescription();
				Capabilities capabilities = servDesc.getCapabilities();

				List<CapabilityType> capabilityList = capabilities
						.getCapability();

				for (int j = 0; j < capabilityList.size(); j++) {
					FrameworkEU framework = getFramework(capabilityList.get(j));
					List<FrameworkEU> frameworks = new ArrayList<FrameworkEU>();
					frameworks.add(framework);
					CapabilityEU capability = new CapabilityEU(capabilityList
							.get(j).getId(), capabilityList.get(j).getName(),
							capabilityList.get(j).getDescription(), frameworks);
					listCapabilityEU.add(capability);
				}
				capabilitiesEU = new CapabilitiesEU(agreement.getName(), listCapabilityEU);
			}
		}
		return capabilitiesEU;
	}

	private CapabilitiesEU getCapabilities(AgreementOffer agreement,
			List<IdList> ids) {
		List<CapabilityEU> listCapabilityEU = new ArrayList<CapabilityEU>();
		All all = agreement.getTerms().getAll();
		List<Term> terms = all.getAll();
		CapabilitiesEU capabilitiesEU = null;
		for (int i = 0; i < terms.size(); i++) {
			if (terms.get(i) instanceof GuaranteeTerm) {
				GuaranteeTerm g = (GuaranteeTerm) terms.get(i);
			} else if (terms.get(i) instanceof ServiceDescriptionTerm) {
				ServiceDescriptionTerm serviceDescTerm = (ServiceDescriptionTerm) terms
						.get(i);
				ServiceDescriptionType servDesc = serviceDescTerm
						.getServiceDescription();
				Capabilities capabilities = servDesc.getCapabilities();
				List<CapabilityType> capabilityList = capabilities
						.getCapability();

				for (int j = 0; j < capabilityList.size(); j++) {
					FrameworkEU framework = getSecurities(capabilityList.get(j));
					for (int k = 0; k < ids.size(); k++) {
						if (capabilityList.get(j).getId()
								.equals(ids.get(k).getId())) {
							List<FrameworkEU> frameworks = new ArrayList<FrameworkEU>();
							frameworks.add(framework);
							CapabilityEU capability = new CapabilityEU(
									capabilityList.get(j).getId(),
									capabilityList.get(j).getName(),
									capabilityList.get(j).getDescription(),
									frameworks);
							listCapabilityEU.add(capability);
						}
					}
				}
				capabilitiesEU = new CapabilitiesEU(agreement.getName(), listCapabilityEU);
			}
		}
		return capabilitiesEU;
	}
}
