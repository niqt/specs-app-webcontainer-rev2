package specs.sws_application.frontend.rest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class WebController {
	@RequestMapping("/index")
    public ModelAndView helloWorld(Model model) {
        model.addAttribute("message", "Hello World!");
        return new ModelAndView("index", "message", model);
    }
	
	
	@RequestMapping("/welkome")
    public ModelAndView hello(Model model) {
        model.addAttribute("message", "Hello World!");
        return new ModelAndView("welkome", "message", model);
    }
}
