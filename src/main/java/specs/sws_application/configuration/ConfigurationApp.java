package specs.sws_application.configuration;

import javax.annotation.Resource;
import javax.inject.Inject;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;



@Configuration
@ComponentScan("specs.sws_application.configuration")
@PropertySource("classpath:sws_conf.properties")
public class ConfigurationApp {
	@Autowired
	public Environment env;
	
	public String getServicesUrl() {
		if (env != null)
		return (String) env.getProperty("sla_manager.endpoint");
		return "";
	}

}
