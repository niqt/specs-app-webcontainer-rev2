package specs.sws_application.utility;

import java.util.List;

import eu.specs.datamodel.sla.sdt.OneOpOperator;
import specs.sw_application.frontend.entities.SecurityControlEU;

public class Utility {
	public static String lastElement(String url, String separator) {

		if (url.contains(separator) && !url.endsWith(separator)) {
			String [] tokens = url.split(separator);
			if (tokens.length > 0)
				return tokens[tokens.length - 1];
		}
		return "";
	}
	
	public static String secondLastElement(String url, String separator) {

		return url.substring(0, url.lastIndexOf("_"));
	}
	
	public static boolean containSecurityControl(List<SecurityControlEU> securities, String id) {
		for (int i = 0; i < securities.size(); i++) {
			if (securities.get(i).getId().equals(id))
				return true;
		}
		return false;
	}
	
	public static String getSLOOperand(String op) {
			 if (op.equals("EQUAL"))
				 return "eq";
			 else if (op.equals("LESS THAN"))
				 return "lt";
			 else if (op.equals("GREATER THAN"))
				 return "gt";
			 else if (op.equals("LESS OR EQUAL THAN"))
				 return "le";
			 else if (op.equals("GREATER OR EQUAL THAN"))
				 return "ge";
			 else
				 return "";
	}
	
	public static OneOpOperator convertTOSLOOperator(String op) {
		 if (op.equals("eq"))
			 return OneOpOperator.EQUAL;
			 
		 else if (op.equals("lt"))
			 return OneOpOperator.LESS_THEN;
		 else if (op.equals("gt"))
			 return OneOpOperator.GREATER_THEN;
		 else if (op.equals("le"))
			 return OneOpOperator.LESS_THEN_OR_EQUAL;
		 else if (op.equals("ge"))
			 return OneOpOperator.GREATER_THEN_OR_EQUAL;
		 return OneOpOperator.EQUAL;
}
	
	public static String getFromQuotes(String op) {
		String result = op.substring(
				op.indexOf("'") + 1,
				op.lastIndexOf("'"));
		return result;
	}
}
