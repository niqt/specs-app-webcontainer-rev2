package specs.sw_application.frontend.entities;

import java.util.List;

public class FrameworkEU {
	private String id;
	private String name;
	private List<SecurityControlEU> securityControls;
	
	public FrameworkEU(String id2, String frameworkName,
			List<SecurityControlEU> securityControl2) {
		this.id = id2;
		this.name = frameworkName;
		this.securityControls = securityControl2;
	}
	
	public FrameworkEU() {

	}
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public List<SecurityControlEU> getSecurityControls() {
		return securityControls;
	}
	public void setSecurityControls(List<SecurityControlEU> securityControl) {
		this.securityControls = securityControl;
	}
	
}
