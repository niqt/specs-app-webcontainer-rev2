package specs.sw_application.frontend.entities;

public class ServiceEU {
	private String id;

	public ServiceEU(String id) {
		this.id = id;
	}
	
	public ServiceEU() {
		
	}
	
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}
	
}
