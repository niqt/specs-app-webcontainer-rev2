package specs.sw_application.frontend.entities;

import java.util.List;

public class CapabilitySLOStep {
	String id;
	String name;
	List<FrameworkSLO> frameworks;
	
	public CapabilitySLOStep(String id, String name, List<FrameworkSLO> frameworks) {
		this.id = id;
		this.name = name;
		this.frameworks = frameworks;
	}
	
	public CapabilitySLOStep() {
		// TODO Auto-generated constructor stub
	}

	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public List<FrameworkSLO> getFrameworks() {
		return frameworks;
	}
	public void setFrameworks(List<FrameworkSLO> frameworks) {
		this.frameworks = frameworks;
	}
}
