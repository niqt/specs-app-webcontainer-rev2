package specs.sw_application.frontend.entities;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

public class IdList {
	@JsonProperty("id")
	public String id;
	
	public IdList() {
		
	}
	
	public IdList(String ids) {
		this.id = ids;
	}
	
	public String getId() {
		return id;
	}

	public void setId(String ids) {
		this.id = ids;
	}
}
