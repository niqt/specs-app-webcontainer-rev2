package specs.sw_application.frontend.entities;

import java.util.List;

public class ServicesEU {
	List<ServiceEU> services;
	
	public List<ServiceEU> getServices() {
		return services;
	}

	public void setServices(List<ServiceEU> services) {
		this.services = services;
	}

	public ServicesEU(List<ServiceEU> services) {
		this.services = services;
	}
	
	public ServicesEU() {
		
	}
}
