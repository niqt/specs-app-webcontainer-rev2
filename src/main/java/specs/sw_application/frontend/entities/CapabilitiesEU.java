package specs.sw_application.frontend.entities;

import java.util.List;

public class CapabilitiesEU {
	private String idsla;
	private List<CapabilityEU> capabilities;
	
	public CapabilitiesEU(String idsla, List<CapabilityEU> capabilities) {
		this.idsla = idsla;
		this.capabilities = capabilities;
	}
	
	public CapabilitiesEU() {
		
	}
	
	public String getIdsla() {
		return idsla;
	}
	public void setIdsla(String idsla) {
		this.idsla = idsla;
	}
	public List<CapabilityEU> getCapabilities() {
		return capabilities;
	}
	public void setCapabilities(List<CapabilityEU> capabilities) {
		this.capabilities = capabilities;
	}

}
