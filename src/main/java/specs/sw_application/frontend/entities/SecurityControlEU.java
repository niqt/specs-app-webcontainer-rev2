package specs.sw_application.frontend.entities;

import java.util.ArrayList;
import java.util.List;

import eu.specs.datamodel.sla.sdt.WeightType;

public class SecurityControlEU {
	private String description;
	private String id;
	private String name;
	private String weight;
	private List<String> weights; 
	private List<MetricSLO> metrics;
	
	public SecurityControlEU(String id,String name, String description, String weight) {
		this.description = description;
		this.id = id;
		this.name = name;
		this.weight = weight;
		weights = new ArrayList<String>();
		weights.add("LOWER");
		weights.add("MEDIUM");
		weights.add("HIGH");
		setMetrics(new ArrayList<MetricSLO>());
	}
	
	public SecurityControlEU() {
		weights = new ArrayList<String>();
		weights.add("LOWER");
		weights.add("MEDIUM");
		weights.add("HIGH");
		setMetrics(new ArrayList<MetricSLO>());
	}
	
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getWeight() {
		return weight;
	}
	public void setWeight(String weight) {
		this.weight = weight;
	}
	
	public List<String> getWeights() {
		return weights;
	}

	public List<MetricSLO> getMetrics() {
		return metrics;
	}

	public void setMetrics(List<MetricSLO> metrics) {
		this.metrics = metrics;
	}
}
